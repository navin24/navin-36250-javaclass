package test;

class Person{
	public void print()
	{
		System.out.println("Person.print");
	}
}
class Employee extends Person{
	public void print()
	{
		System.out.println("Employee.print");
	}
}
public class Program {
	public static void main(String[] args) {
		Person p = new Person();
		Employee emp = (Employee)p;
		emp.print();
	}
	
	public static void main5(String[] args) {
		Person p = null;
		Employee emp = (Employee)p;
		System.out.println(emp);
	}
	public static void main4(String[] args) {
		Employee emp = new Employee();
		Person p =(Person)emp;
		p.print();

	}
	public static void main3(String[] args) {
		Person p =new Employee();
		p.print();//Employee.print//Dynamic Method dispatch

	}
	public static void main2(String[] args) {
		Employee emp =new Employee();
		emp.print();//Person.print

	}
	public static void main1(String[] args) {
		Person p =new Person();
		p.print();//Person.print

	}

}

