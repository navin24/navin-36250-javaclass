package test;
class A{
	int num1;
	int num2;
	public int getNum1() {
		return num1;
	}
	public void setNum1(int num1) {
		this.num1 = num1;
	}
	public int getNum2() {
		return num2;
	}
	public void setNum2(int num2) {
		this.num2 = num2;
	}
	//public void setNum3(int num3) {
	//}
	public void print()
	{
		System.out.println("Num1	:	"+this.num1);
		System.out.println("Num2	:	"+this.num2);
	}
};
class B extends A{
	int num3;

	public int getNum3() {
		return num3;
	}
	public void setNum3(int num3) {
		this.num3 = num3;
	}
	public void print()
	{
		super.print();
		System.out.println("Num3	:	"+this.num3);
	}
};

public class Program {
	public static void main(String[] args) {
		A a = new B();
		B b = (B)a;
		a.setNum1(10);
		a.setNum2(20);
		b.setNum3(30);
		a.print();
		
	}
	public static void main3(String[] args) {
		A a = new B();
		B b = (B)a;
		System.out.println("Num1	:	"+a.num1);
		System.out.println("Num2	:	"+a.num2);
		System.out.println("Num3	:	"+b.num3);
	}
	public static void main2(String[] args) {
		B b = new B();
		System.out.println("Num1	:	"+b.num1);
		System.out.println("Num2	:	"+b.num2);
		System.out.println("Num3	:	"+b.num3);
	}
	public static void main1(String[] args) {
		A a = new A();
		System.out.println("Num1	:	"+a.num1);
		System.out.println("Num2	:	"+a.num2);
		//System.out.println("Num3	:	"+a.num3);

	}

}
