package test;

import java.util.Scanner;

class Rectangle
{
	private static Scanner sc = new Scanner(System.in);
	private int length;
	private int breadth;
	private float area;
	
	public Rectangle()
	{
		this.length=0;
		this.breadth=0;
		this.area=0.0f;	
	}
	public Rectangle(int length,int breadth,float area)
	{
		this.length=length;
		this.breadth=breadth;
		this.area=area;		
	}
	public void acceptRect()
	{
		System.out.print("Emter Length	:	");
		this.length = sc.nextInt();
		System.out.print("Emter Breadth	:	");
		this.breadth = sc.nextInt();
	}
	public void printRect()
	{
		this.area = this.length * this.breadth;
		System.out.println("Area of Rectangle	:	"+this.area);
	}
};
class Circle
{
	private static Scanner sc = new Scanner(System.in);
	private int radius;
	private float area;
	
	public Circle()
	{
		this.radius=0;
		this.area=0.0f;	
	}
	public Circle(int radius,float area)
	{
		this.radius=radius;
		this.area=area;		
	}
	public void acceptCircle()
	{
		System.out.print("Emter radius	:	");
		this.radius = sc.nextInt();
	}
	public void printCircle()
	{
		this.area = (float) (Math.PI * Math.pow(this.radius, 2));
		System.out.println("Area of Circle	:	"+this.area);
	}
};

public class Program {

	public static void main(String[] args) {
		Rectangle rect = new Rectangle();
		Circle circle = new Circle();
		rect.acceptRect();
		circle.acceptCircle();
		rect.printRect();
		circle.printCircle();
		
	}

}
