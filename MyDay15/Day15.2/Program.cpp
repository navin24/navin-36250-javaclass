#include<iostream>
#include<string>
using namespace std;
class Product
{
private:
	string title;
	float price;
public:
	Product()
	{
		this->title="";
		this->price=0.0f;
	}
	virtual void acceptRecord()
	{
		cout<<"Title:	";
		cin>>this->title;
		cout<<"Price:	";
		cin>>this->price;
	}
	virtual void printRecord()
	{
		cout<<"Title	:"<<this->title<<endl;
		cout<<"Price	:"<<this->price<<endl;
	}
};
class Book : public Product
{
private:
	int pageCount;
public:
	Book()
	{
		this->pageCount=0;
	}
	void acceptRecord()
	{
		Product::acceptRecord();
		cout<<"Page Count:	";
		cin>>this->pageCount;
	}
	void printRecord()
	{
		Product::printRecord();
		cout<<"Page Count	:"<<this->pageCount<<endl;
	}
};
class Tape : public Product
{
private:
	int playTime;
public:
	Tape()
	{
		this->playTime=0;
											}
	void acceptRecord()
	{
		Product::acceptRecord();
		cout<<"Play Time:	";
		cin>>this->playTime;
	}
	void printRecord()
	{
		Product::printRecord();
		cout<<"Play Time	:"<<this->playTime<<endl;
	}
};
int menuList()
{
	int ch;
	cout<<"0:Exit"<<endl;
	cout<<"1:Book"<<endl;
	cout<<"2:Tape"<<endl;
	cout<<"Enter Choice	:";
	cin>>ch;
	return ch;
}

int main(void)
{
	int choice;
	while((choice=menuList())!=0)
	{
		Product *p = NULL;
		switch(choice)
		{
			case 0:
				break;
			case 1:
				p = new Book();
				break;
			case 2:
				p = new Tape();
				break;
			default:
				cout<<"Invalid Choice"<<endl;
		}
		if(p!=NULL)
		{
			//RunTime Polymorphism
			p->acceptRecord();
			p->printRecord();
			delete p;
		}
	}
}
int main3(void)
{
	int choice;
	while((choice=menuList())!=0)
	{
		Book *book = NULL;
		Tape *tape = NULL;
		switch(choice)
		{
			case 0:
				break;
			case 1:
				book = new Book();
				book->acceptRecord();
				book->printRecord();
				delete book;
				break;
			case 2:
				tape = new Tape();
				tape->acceptRecord();
				tape->printRecord();
				delete tape;
				break;
			default:
				cout<<"Invalid Choice"<<endl;
		}
	}
}

int main1(void)
{
	Tape tape;
	tape.acceptRecord();
	tape.printRecord();
	return 0;
}
int main2(void)
{
	Book book;
	book.acceptRecord();
	book.printRecord();
	return 0;
}
