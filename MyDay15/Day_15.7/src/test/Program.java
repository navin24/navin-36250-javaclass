package test;

class A{
	private int num1;
	private int num2;
	public void setNum1(int num1) {
		this.num1 = num1;
	}
	public void setNum2(int num2) {
		this.num2 = num2;
	}
	public void print( ) {
		System.out.println("Num1	:	"+this.num1);
		System.out.println("Num2	:	"+this.num2);
	}
}
class B extends A{
	private int num3;
	public void setNum3(int num3) {
		this.num3 = num3;
	}
	@Override
	public void print() {
		super.print();
		System.out.println("Num3	:	"+this.num3);
	}
}
public class Program {
	public static void main(String[] args) {
		A a = new B();
		a.setNum1(10);
		a.setNum2(20);
	}
	/*public static void main2(String[] args) {
		B b = new B( );
		b.setNum1(10);
		b.setNum2(20);
		b.setNum3(30);
		b.print();
	}*/

	/*public static void main1(String[] args) {
		A a = new A( );
		a.setNum1(10);
		a.setNum2(20);
		a.print();
	}
	*/
}

