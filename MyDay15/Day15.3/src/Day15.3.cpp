#include<iostream>
#include<string>
using namespace std;

class Product
{
private:
	string title;
	float price;
public:
	//TODO : Define constructor
	Product( void )
	{
		this->title = "";
		this->price = 0;
	}
	//TODO : define acceptRecord() function
	void acceptRecord( void )
	{
		cout<<"Title		:	";
		cin>>this->title;
		cout<<"Price		:	";
		cin>>this->price;
	}
	//TODO : define printRecord() function
	void printRecord( void )
	{
		cout<<"Title		:	"<<this->title<<endl;
		cout<<"Price		:	"<<this->price<<endl;
	}
};
class Book : public Product
{
private:
	int pageCount;
public:
	//TODO : Define constructor
	Book( void )
	{
		this->pageCount = 0;
	}
	//TODO : define acceptRecord() function
	void acceptRecord( void )
	{
		Product::acceptRecord();
		cout<<"Page Count	:	";
		cin>>this->pageCount;
	}
	//TODO : define printRecord() function
	void printRecord( void )
	{
		Product::printRecord();
		cout<<"Page Count	:	"<<this->pageCount<<endl;
	}
};

class Tape : public Product
{
private:
	int playTime;
public:
	//TODO : Define constructor
	Tape( void )
	{
		this->playTime = 0;
	}
	//TODO : define acceptRecord() function
	void acceptRecord( void )
	{
		Product::acceptRecord();
		cout<<"Play Time	:	";
		cin>>this->playTime;
	}
	//TODO : define printRecord() function
	void printRecord( void )
	{
		Product::printRecord();
		cout<<"Play Time	:	"<<this->playTime<<endl;
	}
};
int menu_list( void )
{
	int choice;
	cout<<"0.Exit"<<endl;
	cout<<"1.Book"<<endl;
	cout<<"2.Tape"<<endl;
	cout<<"Enter choice	:	";
	cin>>choice;
	return choice;
}
int main( void )
{
	int choice;
	while( ( choice = menu_list( ) ) != 0 )
	{
		Book ptrBook;
		switch( choice )
		{
		case 1:
			ptrBook.acceptRecord();
			ptrBook.printRecord();
			break;
		case 2:
			break;
		}
	}
	return 0;
}
