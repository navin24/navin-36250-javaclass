package test;

import java.util.Scanner;
class Shape
{
	protected float area;

	public float getArea() {
		return area;
	}
};
class Rectangle extends Shape
{
	private static Scanner sc = new Scanner(System.in);
	private int length;
	private int breadth;
	
	public Rectangle()
	{
		this.length=0;
		this.breadth=0;	
	}
	public Rectangle(int length,int breadth,float area)
	{
		this.length=length;
		this.breadth=breadth;		
	}
	public void acceptRect()
	{
		System.out.print("Enter Length	:	");
		this.length = sc.nextInt();
		System.out.print("Enter Breadth	:	");
		this.breadth = sc.nextInt();
	}
	public void calcArea()
	{
		this.area=this.length * this.breadth;
	}
};
class Circle extends Shape
{
	private static Scanner sc = new Scanner(System.in);
	private int radius;
	
	public Circle()
	{
		this.radius=0;	
	}
	public Circle(int radius,float area)
	{
		this.radius=radius;		
	}
	public void acceptCircle()
	{
		System.out.print("Emter radius	:	");
		this.radius = sc.nextInt();
	}
	public void calcArea()
	{
		this.area=(float) (Math.PI * Math.pow(this.radius, 2));
	}
};

public class Program {

	public static int menuList()
	{
		Scanner sc = new Scanner(System.in);
		int ch;
		System.out.println("0:Exit");
		System.out.println("1:Rectangle");
		System.out.println("2:Circle");
		System.out.print("Enter choice	:	");
		ch = sc.nextInt();
		return ch;
		
	}
	public static void main(String[] args) {
		
		int ch;
		while((ch=Program.menuList())!=0)
		{
			Shape sp = null;
			switch(ch)
			{
			case 0:
				break;
			case 1:
				sp = new Rectangle();
				Rectangle rect = (Rectangle)sp;
				rect.acceptRect();
				rect.calcArea();
				System.out.println("Area of rectangle	:	"+rect.getArea());
				
				break;
			case 2:
				sp = new Circle();
				Circle c = (Circle)sp;
				c.acceptCircle();
				c.calcArea();
				System.out.println("Area of rectangle	:	"+c.getArea());
				
				break;
			}
		}
	}
}
