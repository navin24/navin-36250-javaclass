#include<iostream>
#include<string>
using namespace std;
class Product
{
private:
	string title;
	float price;
public:
	Product()
	{
		this->title="";
		this->price=0.0f;
	}
	void acceptRecord()
	{
		cout<<"Title:	";
		cin>>this->title;
		cout<<"Price:	";
		cin>>this->price;
	}
	void printRecord()
	{
		cout<<"Title	:"<<this->title<<endl;
		cout<<"Price	:"<<this->price<<endl;
	}
};
class Book : public Product
{
private:
	int pageCount;
public:
	Book()
	{
		this->pageCount=0;
	}
	void acceptRecord()
	{
		Product::acceptRecord();
		cout<<"Page Count:	";
		cin>>this->pageCount;
	}
	void printRecord()
	{
		Product::printRecord();
		cout<<"Page Count	:"<<this->pageCount<<endl;
	}
};
class Tape : public Product
{
private:
	int playTime;
public:
	Tape()
	{
		this->playTime=0;
											}
	void acceptRecord()
	{
		Product::acceptRecord();
		cout<<"Play Time:	";
		cin>>this->playTime;
	}
	void printRecord()
	{
		Product::printRecord();
		cout<<"Play Time	:"<<this->playTime<<endl;
	}
};


int main1(void)
{
	Tape tape;
	tape.acceptRecord();
	tape.printRecord();
	return 0;
}
int main(void)
{
	Book book;
	book.acceptRecord();
	book.printRecord();
	return 0;
}
