#include<stdio.h>
class Complex
{
private:
	int real;
	int imag;
public:
	//Complex *const this = &c1
	Complex( void )	//Constructor
	{
		printf("Inside constructor\n");
		this->real = 0;
		this->imag = 0;
	}
	//Complex *const this = &c1
	void acceptRecord( void )
	{
		printf("Real Number	:	");
		scanf("%d", &this->real);
		printf("Imag Number	:	");
		scanf("%d", &this->imag);
	}
	//Complex *const this = &c1
	void printRecord( void )
	{
		printf("Real Number	:	%d\n", this->real);
		printf("Imag Number	:	%d\n", this->imag);
	}
};
int main( void )
{
	Complex c1;
	//c1.Complex( ); //Not OK

	Complex *ptr = &c1;
	//ptr->Complex( );	//Not OK

	Complex &c2 = c1;
	//c2.Complex( );	//Not OK
	return 0;
}
