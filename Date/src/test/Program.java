/*package test;

public class Date {

}
*/
package test;

import java.time.LocalDate;

class Program{
	private int day, month, year;
	public Program() {
		LocalDate ld = LocalDate.now();
		this.day = ld.getDayOfMonth();
		this.month = ld.getMonthValue();
		this.year = ld.getYear();
	}
	public Program(int day, int month, int year) {
		this.day = day;
		this.month = month;
		this.year = year;
	}
	public void printRecord( ) {
		System.out.println(this.day+" / "+this.month+" / "+this.year);
	}
}
public class Program {
	public static void main(String[] args) {
		Program dt = new Program();
		dt.printRecord();
	}
}