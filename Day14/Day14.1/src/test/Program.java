package test;

//Super class
class Person{
	private String name;
	private int age;
	public Person() {
		System.out.println("Pless");
	}
	public Person(String name, int age) {
		System.out.println("Param");
		this.name = name;
		this.age = age;
	}
	public void printRecord( ) {
		System.out.println("Name	:	"+this.name);
		System.out.println("Age	:	"+this.age);
	}
}
//Sub class
class Employee extends Person{
	private int empid;
	private float salary;
	public Employee() {
		System.out.println("public Employee()");
	}
	public Employee( String name, int age, int empid, float salary ){
		super(name,age);	//Super Statement
		System.out.println("public Employee(abc)");
		this.empid = empid;
		this.salary = salary;
	}
	public void printRecord( ) {
		super.printRecord( );
		System.out.println("Empid	:	"+this.empid);
		System.out.println("Salary	:	"+this.salary);
	}
}
public class Program {
	public static void main(String[] args) {
		Employee emp = new Employee("Sandeep",36, 33, 45000.50f);
		emp.printRecord();
	}
	public static void main3(String[] args) {
		Employee emp = new Employee();
	}
	public static void main2(String[] args) {
		Person p = new Person("ABC", 23);
	}
	public static void main1(String[] args) {
		Person p = new Person( );
	}
}
