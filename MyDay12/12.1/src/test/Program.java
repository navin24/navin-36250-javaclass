/*package test;

public class Program {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	}

}*/
package test;
//Encapsulation
class Array{
	private int[] arr;//Data Hiding
	public Array( ) {
		this(5);
	}
	public Array( int size ) {
		if( size > 0 )
			this.arr = new int[ size ];
		else
			throw new IllegalArgumentException("Invalid size");
	}
	public int getElement( int index ) {
		int element = 0;
		if( this.arr != null ) {
			if( index >= 0 && index < this.arr.length)
				element = this.arr[ index ];
			else
				throw new ArrayIndexOutOfBoundsException("Invalid index.");
		}
		return element;
	}
	public void setElement( int index, int element ) {
		if( this.arr != null ) {
			if( index >= 0 && index < this.arr.length)
				this.arr[ index ] = element;
			else
				throw new ArrayIndexOutOfBoundsException("Invalid index.");
		}
	}
	public int getSize( )
	{
		if( this.arr != null )
			return this.arr.length;
		return 0;
	}
	public void setSize( int size )
	{
		this.arr = new int[ size ];
	}
}
public class Program {
	//Abstraction
	public static void main(String[] args) {
	
		Array a1  = new Array();
		a1.setSize(3);
		
		a1.setElement(0, 1);
		a1.setElement(1, 2);
		a1.setElement(2, 3);
		
		for( int index = 0; index < a1.getSize(); ++ index )
			System.out.println(a1.getElement(index));
		
	}
}