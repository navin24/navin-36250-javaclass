class Date{
    private int day;
    private int month;
    private int year;
    //Constructor
}
class Program{
    public static void main(String[] args) {
        Date dt1 = new Date();
        Date dt2 = dt1; ///Shallow Copy of reference
        Date dt3 = new Date();
        //new Date( );    //Anonymous instance
    }
    public static void main2(String[] args) {
        Date dt = new Date();
    }
    public static void main1(String[] args) {
        //Date dt;    //C++ : object
        Date dt;    //Java : Object reference / reference 
        dt = new Date( );
    }
}