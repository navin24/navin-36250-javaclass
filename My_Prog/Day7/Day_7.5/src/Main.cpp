#include<stdio.h>
void print( void )
{
	//static int count; //BSS Segment
	static int count = 0; //Data Segment
	++ count;
	printf("Count	:	%d\n", count);
}
int main( void )
{
	print();	//1
	print();	//2
	print();	//3
	return 0;
}
