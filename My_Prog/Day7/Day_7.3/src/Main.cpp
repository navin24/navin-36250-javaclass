#include<stdio.h>
class Complex
{
private:
	int real;
	int imag;
private:
	//Complex *const this = &c1
	Complex( void )	//Constructor
	{
		printf("Inside constructor\n");
		this->real = 0;
		this->imag = 0;
	}
public:
	//Complex *const this = &c1
	void acceptRecord( void )
	{
		printf("Real Number	:	");
		scanf("%d", &this->real);
		printf("Imag Number	:	");
		scanf("%d", &this->imag);
	}
	//Complex *const this = &c1
	void printRecord( void )
	{
		printf("Real Number	:	%d\n", this->real);
		printf("Imag Number	:	%d\n", this->imag);
	}
public:
	static void test( void )
	{
		Complex c2;
	}
};
int main( void )
{
	Complex c1;

	Complex::test( );
	return 0;
}
