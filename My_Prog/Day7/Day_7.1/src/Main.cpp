#include<stdio.h>

struct Point
{
	int xPos;//Declaration
	int yPos;//Declaration
};
int main( void )
{
	return 0;
}
int main4( void )
{
	//extern int number;	//Declaration
	//extern int number = 10; //Not OK

	//extern int number;	//OK
	//printf("Number	:	%p\n", &number );
	return 0;
}
//int number = 10;

int main3( void )
{
	void print( void );	//Local Function Declaration

	//print( );	//Function Call : Linker Error

	return 0;
}

int num1 = 10;	//Declaration & Definition
int main2( void )
{
	printf("Num1	:	%d\n", num1 );	//OK : 10
	extern int num2;	//Declaration
	printf("Num2	:	%d\n", num2 );	//OK : 20
	extern int num3;	//Declaration
	printf("Num3	:	%d\n", num3 );	//OK : 30
	return 0;
}
int num2 = 20;	//Declaration & Definition


int main1( void )
{
	int num1;	//Declaration & Definition
	printf("Address of num1	:	%p\n", &num1);

	int num2 = 10;	//Declaration & Definition
	printf("Address of num2	:	%p\n", &num2);
	return 0;
}
