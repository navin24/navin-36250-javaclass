#include<stdio.h>


//void print( void );	//Global Function Declaration
int main( void )
{
	void print( void );	//Local Function Declaration
	print( );

	//void display( void );
	//display( );	//Linker Error
	return 0;
}
int main1( void )
{
	extern int num1;
	printf("Num1	:	%d\n", num1);

	extern int num2;
	//printf("Num2	:	%d\n", num2);	//Linker Error
	return 0;
}
