#include<stdio.h>

int num1 = 10;			//Non static global variable : Program Scope
static int num2 = 20;	//static global variable : File scope

void print( void )
{
	printf("Num1	:	%d\n", num1 );
	printf("Num2	:	%d\n", num2 );
}

static void display( void )
{
	printf("Num1	:	%d\n", num1 );
	printf("Num2	:	%d\n", num2 );
}
