#include<stdio.h>
class Complex
{
private:
	int real;
	int imag;
public:
	//Complex *const this = &c1
	Complex( int real, int imag )	//Constructor
	{
		printf("Inside constructor\n");
		this->real = real;
		this->imag = imag;
	}
	//Complex *const this = &c1
	void printRecord( void )
	{
		printf("Real Number	:	%d\n", this->real);
		printf("Imag Number	:	%d\n", this->imag);
	}
};
int main( void )
{
	Complex c1(10,20);

	return 0;
}
