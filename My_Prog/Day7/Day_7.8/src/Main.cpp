#include<stdio.h>
class Test
{
private:
	 int num1;	//Instance Variable
	 int num2;	//Instance Variable
	static int num3;	//Class Level Variable
public:
	Test( int num1, int num2 )
	{
		this->num1 = num1;
		this->num2 = num2;
		//Test::num3 = 500; //OK : Not Recommended.
	}
	void printRecord( void )
	{
		printf("Num1	:	%d\n", this->num1 );
		printf("Num2	:	%d\n", this->num2 );
		printf("Num3	:	%d\n", Test::num3 );
		printf("\n");
	}
};
int Test::num3 = 500;	//Global Definition <= Provide it to get memory for num3;
int main( void )
{
	Test t1(10,20);
	t1.printRecord( );

	Test t2(30,40);
	t2.printRecord( );

	Test t3(50,60);
	t3.printRecord( );
	return 0;
}
