package test;

class Employee{
	private String name;	//null;
	private int empid;		//0
	private float salary;	//0.0
	public Employee() {
	}
	public Employee(String name, int empid, float salary) {
		this.name = name;
		this.empid = empid;
		this.salary = salary;
		}
		public String getName() {
		return name;
		}
		public void setName(String name) {
		this.name = name;
		}
		public int getEmpid() {
		return empid;
		}
		public void setEmpid(int empid) {
			this.empid = empid;
		}
		public float getSalary() {
			return salary;
		}
		public void setSalary(float salary) {
			this.salary = salary;
		}
}
public class Program {
		private static void printRecord(Employee emp) {
					if( emp != null ){
						System.out.printf("%-15s%-5d%-10.2f\n", emp.getName(), emp.getEmpid(), emp.getSalary());
					}
		}
		public static void main1(String[] args) {
			Employee[] arr = new Employee[ 5 ];
			arr[ 0 ] = new Employee("Prashant",45, 37000.50f );
			arr[ 1 ] = new Employee("Amit",87, 47000.50f );
			arr[ 2 ] = new Employee("Ganesh",12, 57000.50f );
			arr[ 3 ] = new Employee("Devendra",30, 67000.50f );												
			arr[ 4 ] = new Employee("Abhijit",67, 77000.50f );
			for( int index = 0; index < arr.length; ++ index )
				Program.printRecord(arr[ index ] );
		}
		public static void main(String[] args) {
		Employee e1 = new Employee("Prashant",45, 37000.50f );
		Program.printRecord(e1);
		Employee e2 = new Employee("Amit",87, 47000.50f );
		Program.printRecord(e2);
		Employee e3 = new Employee("Ganesh",12, 57000.50f );
		Program.printRecord(e3);
		Employee e4 = new Employee("Devendra",30, 67000.50f );
		Program.printRecord(e4);
		Employee e5 = new Employee("Abhijit",67, 77000.50f );
		Program.printRecord(e5);
		}
}
