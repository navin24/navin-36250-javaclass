package test;
class Date{
	private int day;
	private int month;
	private int year;
	public Date() {
		this( 0,0,0);
	}
	public Date(int day, int month, int year) {
		this.day = day;
		this.month = month;
		this.year = year;
	}
	public int getDay() {
		return day;
	}
	public void setDay(int day) {
		this.day = day;
	}
	public int getMonth() {
		return month;
	}
	public void setMonth(int month) {
		this.month = month;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
}
class Employee{
	private String name;	//null
	private int empid;	//0
	private float salary;	//0.0
	private Date joinDate;	//null
	public Employee() {
		this.name = new String( );
		this.joinDate = new Date();
	}
	public Employee(String name, int empid, float salary, int day, int month, int year) {
		this.name = new String(name);
		this.empid = empid;
		this.salary = salary;
		this.joinDate = new Date(day, month, year);
	}
	
	public Employee(String name, int empid, float salary, Date joinDate) {
		this.name = name;
		this.empid = empid;
		this.salary = salary;
		this.joinDate = joinDate;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getEmpid() {
		return empid;
	}
	public void setEmpid(int empid) {
		this.empid = empid;
	}
	public float getSalary() {
		return salary;
	}
	public void setSalary(float salary) {
		this.salary = salary;
	}
	public Date getJoinDate() {
		return joinDate;
	}
	public void setJoinDate(Date joinDate) {
		this.joinDate = joinDate;
	}
	
}
public class Program {
	public static void main(String[] args) {
		//Employee emp = new Employee("Sandeep",33,12345.50f,26,12,2006);
		
		//Date dt = new Date(26, 12, 2006);
		//Employee emp = new Employee("Sandeep",33,12345.50f,dt);
		
		Employee emp = new Employee("Sandeep",33,12345.50f,new Date(26, 12, 2006));
		
		System.out.println(emp.getName());
		System.out.println(emp.getEmpid());
		System.out.println(emp.getSalary());
		System.out.println(emp.getJoinDate().getDay()+"/"+emp.getJoinDate().getMonth()+"/"+emp.getJoinDate().getYear());
	}
	public static void main1(String[] args) {
		Date joinDate = new Date(26,12,2006);
	}
}

/*package test;

public class Program {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}*/
