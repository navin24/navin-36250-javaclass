#include<iostream>
using namespace std;


class Base
{
private:
	int num1;
	int num2;
public:
	Base( void )
	{
		this->num1 = 10;
		this->num2 = 20;
	}
	Base( int num1, int num2 )
	{
		this->num1 = num1;
		this->num2 = num2;
	}
	void showRecord( void )
	{
		cout<<"Num1	:	"<<this->num1<<endl;
		cout<<"Num2	:	"<<this->num2<<endl;
	}
	void printRecord( void )
	{
		cout<<"Num1	:	"<<this->num1<<endl;
		cout<<"Num2	:	"<<this->num2<<endl;
	}
};
class Derived : public Base
{
private:
	int num3;
public:
	Derived( void )
	{
		this->num3 = 30;
	}
	Derived( int num1, int num2, int num3 ) : Base( num1, num2 )
	{
		this->num3 = num3;
	}
	void displayRecord( void )
	{
		Base::showRecord();
		cout<<"Num3	:	"<<this->num3<<endl;
	}
	void printRecord( void )
	{
		Base::printRecord();
		cout<<"Num3	:	"<<this->num3<<endl;
	}
};
int main(void)
{

}
int main10(void)
{
	Base *ptrBase = new Derived();
	Derived *ptrDerived = (Derived*)ptrBase;  //Down_casting (Explict type casting is MUST)
	ptrDerived->printRecord();
}
int main9(void)
{
	Base *ptrBase = new Derived();
	ptrBase->printRecord();
}
int main8(void)
{
	Derived *ptrDerived =new Derived();
	ptrDerived->printRecord();
	Base *ptrBase = (Base *)ptrDerived;  //UpCasting (No need of Explicit Type casting)
	ptrBase->printRecord();
//	delete ptrDerived;
	return 0;
}
int main7(void)
{
	Derived derived(50,60,70);
	Base b1(1,2);
	b1.printRecord();
	Base base;
	base=derived;
	base.showRecord();
}
int main6(void)
{
	Derived d1(10,20,30);
	Derived d2=d1;
	d2.printRecord();
}
int main5(void)
{
	Base b1(1,2);
	Base b2=b1;
	b2.printRecord();

	return 0;
}
int main4(void)
{
	Derived *ptrDerived=new Derived();
	//ptrDerived->showRecord();
	//ptrDerived->printRecord();
	//ptrDerived->Base::printRecord();
	ptrDerived->Base::showRecord();
	delete ptrDerived;
	return 0;
}
int main3(void)
{
	Derived derived;
//	derived.displayRecord();
//	derived.Base::printRecord();
	derived.Base::showRecord();
}
/*int main2( void )
{
	Base *ptrBase = new Base( );
	//ptrBase->showRecord();	//Base::showRecord();
	//ptrBase->printRecord( );	//Base::printRecord()
	//								//ptrBase->Derived::printRecord( );	//Not OK
	//								//base.displayRecord( ); //Not OK
	//								delete ptrBase;
	//								return 0;
	//								}
	//								//					int main1( void )
	//								//					{
	//								//						Base base;
	//								//							//base.showRecord();	//Base::showRecord();
	//								//								//base.printRecord( );	//Base::printRecord()
	//								//									//base.Derived::printRecord( );	//Not OK
	//								//										//base.displayRecord( ); //Not OK
	//								//											return 0;
	//								//											}
*/	
