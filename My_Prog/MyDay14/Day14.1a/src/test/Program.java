package test;

class Person{

	private String name;
	private int age;
	public Person()
	{
	
	}
	public Person(String name,int age)
	{
		this.name=name;
		this.age=age;
	}
	public void printRecord() {
		// TODO Auto-generated method stub
		System.out.println("Name: "+this.name);
		System.out.println("Age: "+this.age);
		
	}
	
}

public class Program
{
	public static void main(String[] args) {
		Person p = new Person("Navin",23);	
		p.printRecord();
	}
}
