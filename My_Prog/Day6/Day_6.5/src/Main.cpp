#include<stdio.h>
#include<string.h>
class Employee
{
private:
	char name[ 30 ];
	int empid;
	float salary;
public:
	// Employee *const this = &emp
	void initEmployee( void )
	{
		strcpy( this->name,"" );
		this->empid = 0;
		this->salary = 0;
	}
	// Employee *const this = &emp
	void initEmployee( char name[ 30 ], int empid, float salary )
	{
		strcpy( this->name, name );
		this->empid = empid;
		this->salary = salary;
	}
	// Employee *const this = &emp
	void acceptRecord( void )
	{
		printf("Name	:	");
		scanf("%s", this->name );

		printf("Empid	:	");
		scanf("%d", &this->empid );

		printf("Salary	:	");
		scanf("%f", &this->salary );
	}
	// Employee *const this = &emp
	char* getName( )	//Inspector/Selector/Getter Function
	{
		return this->name;
	}
	// Employee *const this = &emp
	void setName( const char *name ) //Mutator / Modifier / Setter Function
	{
		strcpy( this->name, name );
	}
	// Employee *const this = &emp
	int getEmpid( )	//Inspector/Selector/Getter Function
	{
		return this->empid;
	}
	// Employee *const this = &emp
	void setEmpid( int empid )	//Mutator / Modifier / Setter Function
	{
		this->empid = empid;
	}
	// Employee *const this = &emp
	float getSalary( )	//Inspector/Selector/Getter Function
	{
		return this->salary;
	}
	// Employee *const this = &emp
	void setSalary( float salary )	//Mutator / Modifier / Setter Function
	{
		if( salary >= 8000 )
			this->salary = salary;
		else
			throw "Invalid salary";
	}
	// Employee *const this = &emp
	void printRecord( void )
	{
		printf("Name	:	%s\n", this->name );
		printf("Empid	:	%d\n", this->empid );
		printf("Salary	:	%f\n", this->salary );
	}
};
int main( void )
{
	Employee emp;

	emp.acceptRecord( ); //"Sandeep",33,30000

	//emp.printRecord( );	//emp.printRecord( &emp );

	char *name =  emp.getName( );
	float salary = emp.getSalary( );	//float salary = emp.getSalary( &emp );
	printf("%s	%f\n",name, salary);
	return 0;
}
