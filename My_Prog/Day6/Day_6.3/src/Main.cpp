#include<stdio.h>
#include<string.h>

class Employee
{
private:
	char name[ 30 ];	//Data Members
	int empid;			//Data Members
	float salary; 		//Data Members
public:
	void acceptRecord(  )
	{
		printf("Name	:	");
		scanf("%s", name );
		printf("Empid	:	");
		scanf("%d", &empid );
		printf("Salary	:	");
		scanf("%f", &salary );
	}
	void printRecord(  )
	{
		printf("Name	:	%s\n", name );
		printf("Empid	:	%d\n", empid );
		printf("Salary	:	%f\n", salary );
	}
};
int main( void )
{
	Employee emp;
	emp.acceptRecord( );	//emp.acceptRecord( &emp );
	emp.printRecord( );		//emp.printRecord( &emp );
	return 0;
}
