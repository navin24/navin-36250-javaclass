#include<stdio.h>
#include<stdlib.h>
struct Employee	//Global structure declaration
{
	char name[ 50 ];
	int empid;
	float salary;
};
void accept_record( struct Employee *ptr ) //Called Function
{
	printf("Name	:	");
	scanf("%s", ptr->name);
	printf("Empid	:	");
	scanf("%d",&ptr->empid);
	printf("Salary	:	");
	scanf("%f", &ptr->salary);
}
void print_record(struct Employee *ptr  )	//Called Function
{
	printf("Name	:	%s\n", ptr->name);
	printf("Empid	:	%d\n", ptr->empid);
	printf("Salary	:	%f\n", ptr->salary);
}
int main( void ) //Calling Function
{

	struct Employee emp;

	accept_record( &emp );	//Function call

	print_record( &emp ); 	//Function call

	return 0;
}
