package test;
public class Program {
	//int printf( const char *format, ...);
	//Variable argument method / variable arity method
	private static void sum(int... args ) {
		int result = 0;
		for( int element : args )
			result = result + element;
		System.out.println("Sum	:	"+result);
	}
	public static void main(String[] args) {
		Program.sum( );
		Program.sum(10, 20 );
		Program.sum(10, 20, 30 );
		Program.sum(10, 20, 30, 40 );
		Program.sum(10, 20, 30, 40, 50  );
		Program.sum(10, 20, 30, 40, 50, 10, 20, 30, 40, 50  );
	}
}
