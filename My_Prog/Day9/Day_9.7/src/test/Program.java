package test;
class Test{
	private int num1;
	private int num2;
	private static int num3;
	private static int num4;
	private static int num5;
	
	//Static Initializer block / Static Block
	static{
		System.out.println("Static Block:1");
		num3 = 500;
	}
	
	public Test() {
	}
	//Static Initializer block / Static Block
	static{
		System.out.println("Static Block:2");
		num4 = 500;
	}
	public Test(int num1, int num2) {
		this.num1 = num1;
		this.num2 = num2;
	}
	//Static Initializer block / Static Block
	static{
		System.out.println("Static Block:3");
		num5 = 500;
	}
}
public class Program {
	public static void main(String[] args) {
		Test t1 = new Test(10,20);
		Test t2 = new Test(30,40);
		Test t3 = new Test(50,60);
	}
}