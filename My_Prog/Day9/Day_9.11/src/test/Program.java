package test;
class Complex{
	private int real;
	private int imag;
	public Complex(int real, int imag) {
		this.real = real;
		this.imag = imag;
	}
	public int getReal() {
		return real;
	}
	public void setReal(int real) {
		this.real = real;
	}
	public int getImag() {
		return imag;
	}
	public void setImag(int imag) {
		this.imag = imag;
	}
}

public class Program {
	public static void main(String[] args) {
		final Complex c1 = new Complex(10, 20);
		c1.setReal(100);
		c1.setImag(200);
		//c1 = new Complex( 50,60);	//Not OK
		System.out.println("Real Number	:	"+c1.getReal()); //100
		System.out.println("Imag Number	:	"+c1.getImag()); //200
	}
	public static void main1(String[] args) {
		Complex c1 = new Complex(10, 20);
		c1 = new Complex(50, 60);
		System.out.println("Real Number	:	"+c1.getReal()); //50
		System.out.println("Imag Number	:	"+c1.getImag()); //60
	}
}