package test;
public class Program {
	static int count = 0;
	public static void print( ) {
		//static int count = 0;	//Not OK
		++ count;
		System.out.println("Count	:	"+count);
	}
	public static void main(String[] args) {
		Program.print(); 	//1
		Program.print();	//2
		Program.print();	//3
	}
}