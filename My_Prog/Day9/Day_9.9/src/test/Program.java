package test;
public class Program {
	public static void main(String[] args) {
		final int number;
		number = 10;	//Assignment : OK
		//number = number + 5; //Not OK
		System.out.println("Number	:	"+number);
	}
	public static void main1(String[] args) {
		final int number = 10;	//Initialization
		//number = number + 5; //Not OK
		System.out.println("Number	:	"+number);
	}
}