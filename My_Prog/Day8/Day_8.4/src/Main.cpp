#include<stdio.h>

class A
{
public:
	static void f1( void )
	{
		printf("A::f1\n");
	}
};
class B
{
public:
	static void f2( void )
	{
		printf("B::f2\n");
	}
	static void f3( void )
	{
		//f1( );	//Not OK
		A::f1( );

		//f2( );	//OK
		B::f2();	//OK
	}
};
int main( void )
{
	//f3( );	//Not OK
	B::f3();	//OK
	return 0;
}
