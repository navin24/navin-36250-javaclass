#include<stdio.h>
#include<stdlib.h>

class Complex
{
private:
	int real;
	int imag;
public:
	Complex( void )
	{
		this->real = 0;
		this->imag = 0;
	}
	Complex( int real, int imag )
	{
		this->real = real;
		this->imag = imag;
	}
	void printRecord( void )
	{
		printf("Real Number	:	%d\n", this->real);
		printf("Imag Number	:	%d\n", this->imag);
	}
};
int main( void )
{
	//Complex *ptr =  ( Complex* )malloc( sizeof( Complex ) );

	//Complex *ptr =  new Complex;
	//Complex *ptr =  new Complex();
	Complex *ptr =  new Complex(10,20);
	ptr->printRecord();
	delete ptr;
	return 0;
}
int main1( void )
{
	Complex *ptr =  ( Complex* )malloc( sizeof( Complex ) );
	ptr->printRecord();
	free( ptr );
	return 0;
}
