package test;

import java.time.LocalDate;
class Date{
	private int day, month, year;
	public Date() {
		LocalDate ld = LocalDate.now();
		this.day = ld.getDayOfMonth();
		this.month = ld.getMonthValue();
		this.year = ld.getYear();
	}
	public Date(int day, int month, int year) {
		this.day = day;
		this.month = month;
		this.year = year;
	}
	public void printRecord( ) {
		System.out.println(this.day+" / "+this.month+" / "+this.year);
	}
}
public class Program {
	public static void main(String[] args) {
		
		Date[] arr = new Date[ 3 ];
		for( int index = 0; index < arr.length; ++ index )
			arr[ index ] = new Date();
			
		for( int index = 0; index < arr.length;++ index )
			arr[ index ].printRecord(); //OK
	}
	public static void main2(String[] args) {
	
		Date[] arr = new Date[ 3 ];
		arr[ 0 ] = new Date();
		arr[ 1 ] = new Date();
		arr[ 2 ] = new Date();
		
		for( int index = 0; index < arr.length;++ index )
			arr[ index ].printRecord(); //OK
	}
	public static void main1(String[] args) {
		//Date dt;	//In C++ : Object
		//Date *dt = new Date();//In C++ : Dynamic Object
		
		//Date arr[ 3 ];	//In C++ : Array of Objects
		//Date *arr = new Date[ 3 ];	//In C++ : Array of Dynamic Objects
		
		//Date dt; //In java : Object reference /reference
		//Date dt = new Date(); //In java : Instance
		
		//Date[] arr;	//reference of array
		//Date[] arr = new Date[ 3 ];	//Array of references;
	
		Date[] arr = new Date[ 3 ];
		for( int index = 0; index < arr.length;++ index )
			arr[ index ].printRecord(); //NullPointerException
	}
}
