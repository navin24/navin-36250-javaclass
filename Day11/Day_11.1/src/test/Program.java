package test;

import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Scanner;
public class Program {
	private static Scanner sc = new Scanner(System.in);
	public static void main16(String[] args) {
		int[] arr = new int[ ] { 10, 20, 30 };
		for( int element : arr )
			System.out.println(element);
	}
	public static void main15(String[] args) {
		LinkedList<Integer> list = new LinkedList<>( );
		list.addLast(10);
		list.addLast(20);
		list.addLast(30);
		
		for( Integer element : list )	//For Each loop / Iterator
			System.out.println(element);
	}
	public static void main14(String[] args) {
		LinkedList<Integer> list = new LinkedList<>( );
		list.addLast(10);
		list.addLast(20);
		list.addLast(30);
		
		//Iterator
		Integer element = null;
		Iterator<Integer> itr = list.iterator();
		while( itr.hasNext()) {
			element = itr.next();
			System.out.println(element);
		}
	}
	public static void main13(String[] args) {
		int[] arr = new int[  ] { 50, 10, 40, 30, 20  };
		System.out.println(Arrays.toString(arr));//[50, 10, 40, 30, 20]
		Arrays.sort(arr);	//Dual-Pivot QuickSort Algorithm
		System.out.println(Arrays.toString(arr));//[10, 20, 30, 40, 50]
	}
	public static void main12(String[] args) {
		int[] arr1 = new int[  ] { 10, 20, 30 };
		
		int[] arr2 = Arrays.copyOf(arr1, arr1.length);
		
		for( int index = 0; index < arr2.length; ++ index )
			arr2[ index ] = arr2[ index ] + 1;
		
		System.out.println(Arrays.toString(arr1));
		System.out.println(Arrays.toString(arr2));
	}
	public static void main11(String[] args) {
		int element;
		int[] arr = new int[  ] { 10, 20, 30 };//OK
		
		element = arr[ 0 ];
		System.out.println(element);
		
		element = arr[ arr.length - 1 ];
		System.out.println(element);
		
		element = arr[ -1 ]; //ArrayIndexOutOfBoundsException
		//element = arr[ arr.length ]; //ArrayIndexOutOfBoundsException
		System.out.println(element);
	}
	public static void main10(String[] args) {
		//int arr[ 3 ] = { 10, 20, 30 };	//OK : C
		//int arr[  ] = { 10, 20, 30 };	//OK C
		
		//int[] arr = new int[ 3 ] { 10, 20, 30 };	//Not Ok : Java
		//int[] arr = new int[ 3 ];//OK
		//int[] arr = new int[  ];//Not OK
		
		//int[] arr = new int[  ] { 10, 20, 30 };//OK
		
		int[] arr = { 10, 20, 30 };//OK
		System.out.println(Arrays.toString(arr));
	}
	public static void main9(String[] args) {
		int[] arr = new int[ -3 ]; //NegativeArraySizeException
	}
	private static void acceptRecord(int[] arr) {
		if( arr != null ) {
			for( int index = 0; index < arr.length; ++ index ) {
				System.out.print("Enter element	:	");
				arr[ index ] = sc.nextInt();
			}
		}
	}
	private static void printRecord(int[] arr) {
		if( arr != null ) {
			for( int index = 0; index < arr.length; ++ index )
				System.out.println(arr[ index ] );
		}
	}
	public static void main8(String[] args) {
		int[] arr = new int[ 3 ];
		Program.acceptRecord( arr );
		//Program.printRecord(arr);
		System.out.println(Arrays.toString(arr));
	}
	public static void main7(String[] args) {
		int[] arr = new int[ 3 ];
		//String str =  Arrays.toString(arr);
		System.out.println(Arrays.toString(arr)); //[0, 0, 0]
		
		int[] arr1 = null;
		System.out.println(Arrays.toString(arr1)); //null
	}
	public static void main6(String[] args) {
		int[] arr1  = new int[ 3 ];
		Program.printRecord( arr1 );
		
		System.out.println("--------");

		int[] arr2  = new int[ 5 ];
		Program.printRecord( arr2 );
		
		int[] arr3  = null;
		Program.printRecord( arr3 );
	}
	public static void main5(String[] args) {
		int[] arr  = new int[ 3 ];
		for( int index = 0; index < 3; ++ index )
			System.out.println(arr[ index ] );
	}
	public static void main4(String[] args) {
		int[] arr  = new int[ 3 ];
	}
	public static void main3(String[] args) {
		int[] arr = null;
		arr = new int[ 3 ];	//OK
	}
	public static void main2(String[] args) {
		int arr[] = null;
		arr = new int[ 3 ];	//OK
	}
	public static void main1(String[] args) {
		//In C Language
		//int arr[ 3 ];
		//int *arr = (int*)malloc( 3 * sizeof(int));
		//int *arr = (int*)calloc( 3 , sizeof(int));
		
		//In C++ Language
		//int arr[ 3 ];
		//int *arr = new int[ 3 ];
		
		//In java
		//new int[ 3 ]; //Anonymous array
		//int arr[ ];	//Array reference : OK
		//int[] arr;	//Array reference : OK
		//int[ arr ];	//NOT OK
	}
}
