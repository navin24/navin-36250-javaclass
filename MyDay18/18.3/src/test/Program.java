package test;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Program {
	public static void main(String[] args) {
		
		Scanner sc = null;
		sc = new Scanner(System.in);
		
		try(Scanner s = new Scanner(System.in))
		{
			System.out.print("Num1	:	");
			int num1 = s.nextInt();
			System.out.print("Num2	:	");
			int num2 = s.nextInt();
			
			if( num2 == 0 ) {
				new ArithmeticException("Divide by zero exception");;
				}
			else {
				int result = num1 / num2;
				System.out.println("Result	:	"+result);	
			}
		}
	}
}