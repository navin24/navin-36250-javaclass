package test;
class Test{
	private int num1;
	private int num2;
	private static int num3;
	static{
		num3 = 500;
	}
	public Test() {
	}
	public Test(int num1, int num2) {
		this.num1 = num1;
		this.num2 = num2;
	}
	public int getNum1() {
		return this.num1;
	}
	public void setNum1(int num1) {
		this.num1 = num1;
	}
	public int getNum2() {
		return this.num2;
	}
	public void setNum2(int num2) {
		this.num2 = num2;
	}
	public static int getNum3() {
		return Test.num3;
	}
	public static void setNum3(int num3) {
		Test.num3 = num3;
	}
}
public class Program {
	public static void printRecord( Test t)
	{
		System.out.println("Num1	:	"+t.getNum1());
		System.out.println("Num2	:	"+t.getNum2());
		System.out.println("Num3	:	"+Test.getNum3());
	}
	public static void main(String[] args) {
		Test t1 = new Test(10,20);
		Program.printRecord(t1);
		
		Test t2 = new Test(30,40);
		Program.printRecord(t2);
		
		Test t3 = new Test(50,60);
		Program.printRecord(t3);
	}
}