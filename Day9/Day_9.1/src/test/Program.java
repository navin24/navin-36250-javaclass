package test;

import java.util.Calendar;

class Date{
	private int day;	//0
	private int month;	//0
	private int year;	//0
	public Date( ) {
		Calendar c = Calendar.getInstance();
		//this.day = c.get( Calendar.DATE ); //OK
		this.day = c.get( Calendar.DAY_OF_MONTH ); //OK
		this.month = c.get(Calendar.MONTH) + 1;
		this.year = c.get(Calendar.YEAR);
	}
	public void printRecord( ) {
		System.out.println(this.day+"/"+this.month+"/"+this.year);
	}
}
public class Program {
	public static void main(String[] args) {
		Date dt = null;
		dt = new Date( );
		dt.printRecord(); //OK
	}
	public static void main5(String[] args) {
		//int number = null; //Not OK
		Date dt = null;	//Local Reference : null reference/null object
		dt.printRecord(); //NullPointerException
	}
	public static void main4(String[] args) {
		Date dt = new Date();
		dt.printRecord();
	}
	public static void main3(String[] args) {
		Date dt;	//Reference
		dt = new Date( );
		dt.printRecord();
	}
	public static void main2(String[] args) {
		String str = "abc";
		int number = Integer.parseInt(str);	 //NumberFormatException
		System.out.println(number);
	}
	public static void main1(String[] args) {
		String str = "125";
		int number = Integer.parseInt(str);	//125
		System.out.println(number);
	}
}