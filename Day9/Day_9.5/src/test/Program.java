package test;
class Test{
	private int num1;
	private int num2;
	private static int num3;
	//Static Initializer block / Static Block
	static{
		System.out.println("Inside static init. block");
		num3 = 500;
	}
	public Test() {
	}
	public Test(int num1, int num2) {
		System.out.println("Inside constructor");
		this.num1 = num1;
		this.num2 = num2;
	}
}
public class Program {
	public static void main(String[] args) {
		Test t1 = new Test(10,20);
		Test t2 = new Test(30,40);
		Test t3 = new Test(50,60);
	}
}