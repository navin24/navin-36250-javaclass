package test;
public class Program {
	public static void main(String[] args) {
		String s1 = "10";
		int n1 = Integer.parseInt(s1);
		
		String s2 = "10.5f";
		float n2 = Float.parseFloat(s2);
		
		String s3 = "3.14";
		double n3 = Double.parseDouble(s3);
		
		String s4 = "true";
		boolean n4 = Boolean.parseBoolean(s4);
	}
}