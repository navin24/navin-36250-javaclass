package test;

class Employee{
	private String name;
	private int empid;
	private float salary;
	public Employee(String name, int empid, float salary) {
		this.name = name;
		this.empid = empid;
		this.salary = salary;
	}
	//Employee this = emp1;
	//Object obj = emp2	//Upcasting
	@Override
	public boolean equals(Object obj) {
		if( obj != null ) {
			Employee other = (Employee) obj;
			if( this.name == other.name)
				return true;
		}
		return false;
	}
}
public class Program {
	public static void main(String[] args) {
		Employee emp1 = new Employee("Sandeep", 33, 15000.50f);
		Employee emp2 = new Employee("Sandeep", 33, 15000.50f);
		//Employee emp2 = null;
		if( emp1.equals(emp2))
			System.out.println("Equal");
		else
			System.out.println("Not Equal");
		//Output : Equal
	}
}
/*package test;

public class Program {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
*/