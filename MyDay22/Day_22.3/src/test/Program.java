package test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class Program {
	public static ArrayList<Integer> getArrayList( ) {
		ArrayList<Integer> list = new ArrayList<Integer>( );
		list.add(10);
		list.add(20);
		list.add(30);
		list.add(40);
		list.add(50);
		return list;
	}
	public static List<Integer> getList( ) {
		List<Integer> list = new ArrayList<Integer>( );
		list.add(10);
		list.add(20);
		list.add(30);
		list.add(40);
		list.add(50);
		return list;
	}
	public static Collection<Integer> getCollection( ) {
		Collection<Integer> list = new ArrayList<Integer>( );
		list.add(10);
		list.add(20);
		list.add(30);
		list.add(40);
		list.add(50);
		return list;
	}
	public static void main(String[] args) {
		Collection<Integer> c = Program.getCollection();
		List<Integer> list = Program.getList();
		ArrayList<Integer> al = Program.getArrayList();
		
		//List<Integer> aList = new ArrayList<>( al ) ; //OK
		//List<Integer> aList = new ArrayList<>( list ) ; //OK
		List<Integer> aList = new ArrayList<>( c ) ; //OK
		
		for( Integer element : aList )
			System.out.println(element);
	}
}
