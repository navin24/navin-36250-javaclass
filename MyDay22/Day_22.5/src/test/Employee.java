package test;

public class Employee implements Comparable<Employee>{
	private int empid;
	private String name, designation, joinDate, department;
	private float salary;
	public Employee() {
	}
	public Employee(int empid, String name, String designation, String joinDate, float salary, String department) {
		this.empid = empid;
		this.name = name;
		this.designation = designation;
		this.joinDate = joinDate;
		this.salary = salary;
		this.department = department;
	}
	public int getEmpid() {
		return empid;
	}
	public void setEmpid(int empid) {
		this.empid = empid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public String getJoinDate() {
		return joinDate;
	}
	public void setJoinDate(String joinDate) {
		this.joinDate = joinDate;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public float getSalary() {
		return salary;
	}
	public void setSalary(float salary) {
		this.salary = salary;
	}
	@Override
	public boolean equals(Object obj) {
		if( obj != null ) {
			Employee other = (Employee) obj;
			if( this.empid == other.empid)
				return true;
		}
		return false;
	}
	@Override
	public int compareTo(Employee other) {
		return this.empid - other.empid;
	}
	@Override
	public String toString() {
		return String.format("%-10s%-8d%-15s%-15s%-15s%-10.2f", this.name, this.empid, this.department, this.designation, this.joinDate, this.salary);
	}
}