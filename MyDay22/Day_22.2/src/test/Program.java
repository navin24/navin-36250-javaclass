package test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Program {
	public static void main(String[] args) {
		List<Integer> list = new ArrayList<>( );
		list.add(10);
		list.add(20);
		list.add(30);
		list.add(40);
		list.add(50);
		
		Integer key = new Integer(30);
		if( list.contains(key)) {
			list.remove(key); //Collection
			System.out.println(key+" is removed");
		}
		else
			System.out.println(key+" not found");
	}
	public static void main5(String[] args) {
		List<Integer> list = new ArrayList<>( );
		list.add(10);
		list.add(20);
		list.add(30);
		list.add(40);
		list.add(50);
		
		Integer key = new Integer(300);
		if( list.contains(key)) {
			int index = list.indexOf(key);
			list.remove(index); //List
			System.out.println(key+" is removed");
		}
		else
			System.out.println(key+" not found");
	}
	public static void main4(String[] args) {
		List<Integer> list = Arrays.asList(10,20,30,40,50);
		//System.out.println(list.getClass().getName()); //Arrays$ArrayList
		for( Integer element : list )
			System.out.println(element);
	}
	public static void main3(String[] args) {
		List<Integer> list = new ArrayList<>( );
		list.add(10);
		list.add(20);
		list.add(30);
		list.add(40);
		list.add(50);
		
		Integer[] arr = new Integer[ list.size() ];
		arr = list.toArray(arr);
		System.out.println(Arrays.toString(arr));
	}
	public static void main2(String[] args) {
		List<Integer> list = new ArrayList<>( );
		list.add(10);
		list.add(20);
		list.add(30);
		list.add(40);
		list.add(50);
		
		Object[] arr = list.toArray();
		System.out.println(Arrays.toString(arr));
	}
	public static void main1(String[] args) {
		List<Integer> list = new ArrayList<>( );
		list.add(50);
		list.add(10);
		list.add(40);
		list.add(20);
		list.add(30);
		
		//Collections.sort(list);
		
		list.sort(null);
		
		for( Integer element : list )
			System.out.print(element+"	");
		System.out.println();
	}
}
