package test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class Program {
	public static List<Integer> getList( ) {
		List<Integer> list = new ArrayList<Integer>( );
		list.add(20);
		list.add(30);
		list.add(40);
		return list;
	}
	public static void main(String[] args) {
		
		List<Integer> keys = Program.getList();
		List<Integer> list = new ArrayList<>( ) ; 
		list.add(10);
		list.add(20);
		list.add(30);
		list.add(40);
		list.add(50);
		list.add(60);
		list.add(70);
		
		if( list.containsAll(keys)) {
			list.retainAll(keys);
			System.out.println("Keys removed");
			for( Integer element : list )
				System.out.println(element);
		}
		else
			System.out.println("Key(s) not found");
		
	}
	public static void main2(String[] args) {
		
		List<Integer> keys = Program.getList();
		List<Integer> list = new ArrayList<>( ) ; 
		list.add(10);
		list.add(20);
		list.add(30);
		list.add(40);
		list.add(50);
		list.add(60);
		list.add(70);
		
		if( list.containsAll(keys)) {
			list.removeAll(keys);
			System.out.println("Keys removed");
			for( Integer element : list )
				System.out.println(element);
		}
		else
			System.out.println("Key(s) not found");
		
	}
	public static void main1(String[] args) {
		List<Integer> al = Program.getList();
		
		List<Integer> list = new ArrayList<>( ) ; 
		list.add(10);
		list.add(50);
		list.add(60);
		list.add(70);
		//list.addAll(al); //OK
		list.addAll(1, al); //ok
		
		for( Integer element : list )
			System.out.println(element);
	}
}
