package test;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class Program {
	public static void main(String[] args) {
		List<Integer> list = new ArrayList<>( );
		list.add(10);
		list.add(20);
		list.add(30);
		list.add(40);
		list.add(50);
		
		Integer element = null;
		ListIterator<Integer> itr = list.listIterator( list.size() );
		while( itr.hasNext()) {
			element = itr.next();
			System.out.print(element+"	");
		}
		System.out.println();
		while( itr.hasPrevious()) {
			element = itr.previous();
			System.out.print(element+"	");
		}
	}
	public static void main9(String[] args) {
		List<Integer> list = new ArrayList<>( );
		list.add(10);
		list.add(20);
		list.add(30);
		list.add(40);
		list.add(50);
		
		Integer element = null;
		ListIterator<Integer> itr = list.listIterator();
		while( itr.hasNext()) {
			element = itr.next();
			System.out.print(element+"	");
		}
		System.out.println();
		while( itr.hasPrevious()) {
			element = itr.previous();
			System.out.print(element+"	");
		}
	}
	public static void main8(String[] args) {
		List<Integer> list = new ArrayList<>( );
		list.add(10);
		list.add(20);
		list.add(400);
		list.add(50);
		list.add(2, 30);
		list.set(3, 40);
		
		Integer element = null;
		Iterator<Integer> itr = list.iterator();
		while( itr.hasNext()) {
			element = itr.next();
			System.out.println(element);
		}
	}
	public static void main7(String[] args) {
		List<Integer> list = new ArrayList<>( );
		list.add(10);
		list.add(20);
		list.add(400);
		list.add(50);
		list.add(2, 30);
		list.set(3, 40);
		
		for( Integer element : list )
			System.out.println(element);
	}
	public static void main6(String[] args) {
		String str = "SunBeam";
		char ch = str.charAt( str.length( ) ); //StringIndexOutOfBoundsException
		System.out.println(6);
	}
	public static void main5(String[] args) {
		int[] arr = new int[ ] { 10, 20, 30 };
		int element = arr[ arr.length ]; //ArrayIndexOutOfBoundsException
		System.out.println(element);
	}
	public static void main4(String[] args) {
		List<Integer> list = new ArrayList<>( );
		list.add(10);
		list.add(20);
		list.add(30);
		
		Integer element = null;
		element = list.get( list.size() ); //IndexOutOfBoundsException
		System.out.println(element);
		
	}
	public static void main3(String[] args) {
		//ArrayList<Integer> list = new ArrayList<>( );
		List<Integer> list = new ArrayList<>( );
		list.add(10);
		list.add(20);
		list.add(30);
		
		Integer element = null;
		for( int index = 0; index < list.size(); ++ index ) {
			element = list.get( index );
			System.out.println(element);
		}
	}
	public static void main2(String[] args) {
		ArrayList<Integer> list = new ArrayList<>( );
		list.add(10);
		list.add(20);
		list.add(30);
		
		int index = 0;
		Integer element = list.get( index );
		System.out.println(element);
	}
	public static void main1(String[] args) {
		ArrayList<Integer> list = new ArrayList<>( );
		list.add(10);
		list.add(20);
		list.add(30);
		
		int count = list.size();
		System.out.println("Count	:	"+count);
	}
}
