#pragma pack(1)
#include<stdio.h>
int main( void )
{
	//Local Structure
	struct Employee	//Declaration of structure
	{
		char name[ 50 ];	//Struct member
		int empid;			//Struct member
		float salary;		//Struct member
	};
	//struct Employee : Data type
	//emp : variable / object
	struct Employee emp;


	printf("Name	:	");
	scanf("%s", emp.name);
	printf("Empid	:	");
	scanf("%d",&emp.empid);
	printf("Salary	:	");
	scanf("%f", &emp.salary);

	printf("Name	:	%s\n", emp.name);
	printf("Empid	:	%d\n", emp.empid);
	printf("Salary	:	%f\n", emp.salary);
	return 0;
}
