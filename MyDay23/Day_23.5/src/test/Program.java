package test;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Queue;

public class Program {
	public static void main(String[] args) {
		Deque<Integer> que = new ArrayDeque<Integer>();
		que.add(20);
		que.add(30);
		que.add(40);
		que.addFirst(10);
		que.addLast(50);
		
		que.removeFirst();
		que.removeLast();
		
		Integer element = null;
		while( !que.isEmpty()) {
			element = que.getFirst();
			System.out.println("Removed element is : "+element);
			que.removeFirst();
		}
	}
	public static void main2(String[] args) {
		Deque<Integer> que = new ArrayDeque<Integer>();
		que.add(20);
		que.add(30);
		que.add(40);
		que.addFirst(10);
		que.addLast(50);
		
		Integer element = null;
		while( !que.isEmpty()) {
			element = que.getLast();
			System.out.println("Removed element is : "+element);
			que.removeLast();
		}
	}
	public static void main1(String[] args) {
		Deque<Integer> que = new ArrayDeque<Integer>();
		que.add(20);
		que.add(30);
		que.add(40);
		que.addFirst(10);
		que.addLast(50);
		
		Integer element = null;
		while( !que.isEmpty()) {
			element = que.getFirst();
			System.out.println("Removed element is : "+element);
			que.removeFirst();
		}
	}
}
