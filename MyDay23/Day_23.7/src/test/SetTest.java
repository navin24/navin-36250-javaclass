package test;

import java.util.Comparator;
import java.util.List;
import java.util.Set;

public class SetTest{
	private Set<Employee> empList; //null
	public void setEmpList(Set<Employee> empList) {
		this.empList = empList;
	}
	//TODO : Add
	public void addEmployees(Employee[] employees) {
		if( this.empList != null && employees != null ) {
			for( Employee emp : employees )
				this.empList.add(emp);
		}
	}
	//TODO : Find
	public Employee findEmployee(int empid) {
		if( this.empList != null ) {
			for( Employee emp : this.empList ) {
				if( emp.getEmpid() == empid )
					return emp;
			}
		}
		return null;
	}
	
	//TODO : Remove
	public boolean removeEmployee(int empid) {
		if( this.empList != null ) {
			Employee key = new Employee();
			key.setEmpid(empid);
			if( this.empList.contains(key)) {
				this.empList.remove(key);
				return true;
			}
		}
		return false;
	}
	//TODO : Print
	public void printEmployees() {
		if( this.empList != null ) {
			if( !this.empList.isEmpty())
			{
				for (Employee emp : empList)
					System.out.println(emp.toString());
			}
			else
				System.out.println("Employee list is empty");
		}
	}
}