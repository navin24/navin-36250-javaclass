package test;

public class Program {
	public static void main(String[] args) {
		Integer n1 = new Integer(10);
		Integer n2 = new Integer(10);
		
		System.out.println(n1.hashCode());
		System.out.println(n2.hashCode());
	}
	public static int getHashCode( int data ) {
		final int PRIME = 151;
		int result = 1;
		result = result * data + PRIME * data;
		return result;
	}
	public static void main2(String[] args) {
		for( int count = 1; count < 500; ++ count ) {
			int data = count;
			int hashcode = Program.getHashCode(count);
			int slot = hashcode % 5;
			System.out.println(data+"	"+hashcode+"	"+slot);
		}
	}
	public static void main1(String[] args) {
		int hashCode;
		
		int x = 10;
		hashCode =  Program.getHashCode(x);
		System.out.println(x+"	"+hashCode);
		
		int y = 15;
		hashCode =  Program.getHashCode(y);
		System.out.println(y+"	"+hashCode);
	}
}
