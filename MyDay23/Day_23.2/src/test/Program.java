package test;

import java.util.ConcurrentModificationException;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.Vector;

public class Program {
	public static void main(String[] args) {
		Vector<Integer> v = new Vector<>( );
		v.add(10);
		v.add(20);
		v.add(30);
		
		Integer element = null;
		Iterator<Integer> itr = v.iterator(); //Fail-Fast 
		while( itr.hasNext()) {
			element = itr.next();
			System.out.println(element);
			if( element == 30 )
				v.add(40); //ConcurrentModificationException
		}
	}
	public static void main1(String[] args) {
		Vector<Integer> v = new Vector<>( );
		v.add(10);
		v.add(20);
		v.add(30);
		
		Integer element = null;
		Enumeration<Integer> e = v.elements();	//Fail-Safe
		while( e.hasMoreElements()) {
			element = e.nextElement();
			System.out.println(element);
			if( element == 30 )
				v.add(40);	//OK
		}
	}
}
