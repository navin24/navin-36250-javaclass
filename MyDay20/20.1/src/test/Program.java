package test;

class Outer{
	private int num1 = 10;	//non static field
	private static int num2 = 20;	//static field
	public void print( ) {
		System.out.println("Num1	:	"+this.num1);
		System.out.println("Num2	:	"+Outer.num2);
	}
}
public class Program {
	public static void main(String[] args) {
		Outer out = new Outer();
		out.print();
	}
}