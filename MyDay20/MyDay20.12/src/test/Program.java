package test;

interface A
{
	int number = 10;
	
	void print();
}
abstract class B implements A
{
	
}
class C implements A
{
	@Override
	public void print() {
		System.out.println("Num:"+number);
		
	}
}


public class Program {

	public static void main(String[] args) {
		A a = new C();
		a.print();
		
		

	}

}
