package client;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.util.Scanner;
public class Program {
	public static final int PORT = 4576;
	public static void main(String[] args) {
		String message = "";
		byte[] buffer = null;
		try( DatagramSocket socket = new DatagramSocket();
			 Scanner sc = new Scanner(System.in);){
			
			while( true ) {
				System.out.print("C:Client	:	");
				message = sc.nextLine();
				buffer = message.getBytes();
				DatagramPacket sendPacket = new DatagramPacket(buffer, buffer.length,InetAddress.getByName("localhost"), PORT);
				socket.send(sendPacket);
				
				buffer =  new byte[ 1024 ];
				DatagramPacket receivePacket = new DatagramPacket(buffer, buffer.length);
				socket.receive(receivePacket);
				message = new String(receivePacket.getData());
				System.out.println("S:Client	:	"+message);
			}
			
		}catch( Exception ex ) {
			ex.printStackTrace();
		}
	}
}
