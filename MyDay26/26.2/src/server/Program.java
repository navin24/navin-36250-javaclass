package server;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.Inet4Address;
import java.util.Scanner;

public class Program {
	public static final int PORT = 4576;
	public static void main(String[] args) {
		String message;
		byte[] buffer = null;
		try( DatagramSocket serverSocket = new DatagramSocket(PORT);
			 Scanner sc = new Scanner(System.in);){
			while( true ){
				buffer = new byte[ 1024 ];
				
				DatagramPacket receivePacket = new DatagramPacket(buffer, buffer.length);
				serverSocket.receive(receivePacket);
				message = new String(receivePacket.getData());
				System.out.println("S:Client	:	"+message);
				
				
				System.out.print("S:Servre	:	");
				message = sc.nextLine();
				buffer = message.getBytes();
				DatagramPacket sendPacket = new DatagramPacket(buffer, buffer.length, receivePacket.getAddress(), receivePacket.getPort());
				serverSocket.send(sendPacket);
			}
			
		}catch( Exception ex ) {
			ex.printStackTrace();
		}
	}
}
