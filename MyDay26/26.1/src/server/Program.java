package server;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class Program {
	public static final int PORT = 5687;

	public static void main(String[] args) {
		try (ServerSocket serverSocket = new ServerSocket(PORT);
			 Socket socket = serverSocket.accept();
			 DataInputStream inputStream = new DataInputStream(new BufferedInputStream(socket.getInputStream()));	
			 DataOutputStream  outputStream = new DataOutputStream(new BufferedOutputStream(socket.getOutputStream()));
			 Scanner sc = new Scanner(System.in);) {
			
			String message = ""; 
			do
			{
				System.out.print("S:Server	:	");
				message = sc.nextLine();
				outputStream.writeUTF(message);
				outputStream.flush();
				
				message = inputStream.readUTF();
				System.out.println("S:Client	:	"+message);
			}while( !message.equalsIgnoreCase("end"));
			
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}
