package test;
class Person
{
	private String name;
	private int age;
	public Person()
	{
		//Paramterless Constructor
	}
	public Person(String name,int age)
	{
		this.name=name;
		this.age=age;
	}
	public void printRecord()
	{
		
		System.out.println("Name:		"+this.name);
		System.out.println("Age:		"+this.age);
	//	System.out.println("Hello,world!!");
	}
};
class Employee extends Person
{
	private int id;
	private String dept;
	
	public Employee()
	{
		//Parameterless Constructor
	}
	public Employee(String name, int age,int id, String dept)
	{
		super(name,age);
		this.id=id;
		this.dept=dept;
	}
	public void printRecord()
	{
		super.printRecord();
		System.out.println("ID:		"+this.id);
		System.out.println("Department:	"+this.dept);
	}
};

public class Program {

	public static void main(String[] args) {
		//Person p1 = new Person("Navin",23);
		//p1.printRecord();
		Employee emp = new Employee("Abc",23,24,"Embedded Developer");
		emp.printRecord();
		//Person.printRecord();//While using fields we don't use class name. We use reference of that class 
	}
}
