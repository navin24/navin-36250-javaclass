#include<iostream>
#include<string>
using namespace std;
class Person
{
private:
	string name;
	int age;
public:
	Person(void)
	{
		this->name="";
		this->age=0;
	}
	Person(string name , int age)
	{
		this->name=name;
		this->age=age;
	}
	void printRecord()
	{
		cout<<"Name:		"<<this->name<<endl;
		cout<<"Age:		"<<this->age<<endl;
	}
};
class Employee:public Person
{
private:
	int id;
	string dept;
public:
	Employee()
	{
		this->id=0;
		this->dept="";
	}
	Employee(string name,int age,int id, string dept):Person(name,age)
	{
		this->id=id;
		this->dept=dept;
	}
	void printRecord()
	{
		Person::printRecord();
		cout<<"ID:		"<<this->id<<endl;
		cout<<"Department:	"<<this->dept<<endl;
	}
};
int main(void)
{
	Person *p = new Person("Navin",23);
	p->printRecord();
	Employee *emp = new Employee("Navin",23,24,"Embedded Developer");	
	emp->printRecord();
	return 0;
}
