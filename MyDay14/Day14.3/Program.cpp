#include<iostream>
#include<string>
using namespace std;
class Person
{
private:
	string name;
	int age;
public:
	Person(void)
	{
		this->name="";
		this->age=0;
	}
	Person(string name , int age)
	{
		this->name=name;
		this->age=age;
	}
	void printPersonRecord()
	{
		cout<<"Name:		"<<this->name<<endl;
		cout<<"Age:		"<<this->age<<endl;
	}
};
class Employee:public Person
{
private:
	int id;
	string dept;
public:
	Employee()
	{
		this->id=0;
		this->dept="";
		
	}
	Employee(string name,int age,int id, string dept):Person(name,age)
	{
		this->id=id;
		this->dept=dept;
	}
	void printEmployeeRecord()
	{
		Person::printPersonRecord();
		cout<<"ID:		"<<this->id<<endl;
		cout<<"Department:	"<<this->dept<<endl;
	}
};
int main(void)
{
	Person *per = new Employee();//DownCasting
	Employee *emp = (Employee*)per;
	emp->printEmployeeRecord();
	return 0;
	
}
int main6(void)
{
	Employee *emp= new Employee();
	emp->printEmployeeRecord();
	Person *per = emp;//UpCasting
	per->printPersonRecord();//Ok
//	per->printEmployeeRecord();//Not Ok
}
int main5(void)
{
	Employee emp("Nain",23,24,"EDeveloper");
	Person per;
	per=emp;
	per.printPersonRecord();//Object Splicing
	//per.printEmployeeRecord();//Not Ok

}
int main4(void)
{
	Person per("Navin",23);
	Person p1=per;
	p1.printPersonRecord();
	Employee emp("Suraj",23,20,"Embedded Developer");
	Employee emp1=emp;
	emp1.printEmployeeRecord();
}
int main3(void)
{
	Employee emp;
	//emp.printPersonRecord();//Ok
	//emp.Person::printPersonRecord();//Ok
	emp.printEmployeeRecord();

}
int main2(void)
{
	Person *ptr = new Person("navin",23);
	ptr->printPersonRecord();//Ok
	//ptr->printEmployeeRecord();//Not Ok
}
int main1(void)
{
	Person per;
	Employee emp;

	per.printPersonRecord();//Ok
//	per.printEmployeeRecord(); Not Ok
//	per.Employee::printEmployeeRecord();//Not Ok
	return 0;
}
