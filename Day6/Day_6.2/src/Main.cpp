#include<stdio.h>
#include<string.h>

struct Employee
{
	//Data Members
//private:
	char name[ 30 ];
	int empid;
	float salary;
//public:
	//Member Functions
	void accept_record(  )
	{
		printf("Name	:	");
		scanf("%s", name );
		printf("Empid	:	");
		scanf("%d", &empid );
		printf("Salary	:	");
		scanf("%f", &salary );
	}
	void print_record(  )
	{
		printf("Name	:	%s\n", name );
		printf("Empid	:	%d\n", empid );
		printf("Salary	:	%f\n", salary );
	}
};
int main( void )
{
	Employee emp;
	emp.accept_record( );	//emp.accept_record( &emp );
	emp.salary = -50000;
	emp.print_record( );	//emp.print_record( &emp );
	return 0;
}
