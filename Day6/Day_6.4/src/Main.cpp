#include<stdio.h>
#include<string.h>
class Employee
{
private:
	char name[ 30 ];
	int empid;
	float salary;
public:
	void initEmployee( /* Employee *const this */)
	{
		strcpy( this->name,"" );
		this->empid = 0;
		this->salary = 0;
	}
	void initEmployee( /* Employee *const this, */ char name[ 30 ], int empid, float salary )
	{
		strcpy( this->name, name );
		this->empid = empid;
		this->salary = salary;
	}
	void acceptRecord( /* Employee *const this */)
	{
		printf("Name	:	");
		scanf("%s", this->name );
		//scanf("%s", name );
		printf("Empid	:	");
		scanf("%d", &this->empid );
		//scanf("%d", &empid );
		printf("Salary	:	");
		scanf("%f", &this->salary );
		//scanf("%f", &salary );
	}
	void printRecord( /*Employee *const this */)
	{
		printf("Name	:	%s\n", this->name );
		printf("Empid	:	%d\n", this->empid );
		printf("Salary	:	%f\n", this->salary );
	}
};
int main( void )
{
	Employee emp;
	//emp.initEmployee( );	//emp.initEmployee( &emp );
	emp.initEmployee( "Sandeep", 33, 25000.45f );	//emp.initEmployee( &emp, "Sandeep", 33, 25000.45f );
	//emp.acceptRecord( ); //emp.acceptRecord( &emp );
	emp.printRecord( );	//emp.printRecord( &emp );
	return 0;
}
