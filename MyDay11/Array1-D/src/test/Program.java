package test;

import java.util.Arrays;
import java.util.Scanner;

public class Program {

	private static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		int arr[] = new int[] {10,20,30};
		for(int elements : arr)
			System.out.println("Element:="+elements);
	}
	public static void main3(String[] args) 	{
		int[] arr1 = new int[] {10,20,30,50,10,44,21,78,90,45,67};
//		int[] arr2 = Arrays.copyOf(arr1, arr1.length);
		System.out.println(Arrays.toString(arr1));
		System.out.println("--------");
		Arrays.sort(arr1);//Dual-Pivot Sort Algorithm
		System.out.println(Arrays.toString(arr1));
		//System.out.println(Arrays.toString(arr2));
	}

	public static void main2(String[] args) 	{
		int[] arr1 = new int[] {10,20,30};
		int[] arr2 = Arrays.copyOf(arr1, arr1.length);
		for(int i=0;i<arr2.length;i++)
		{
			arr2[i]=arr2[i]/5;
		}
		System.out.println(Arrays.toString(arr1));
		System.out.println(Arrays.toString(arr2));
	}

	public static void main1(String[] args) {
		int[] arr = new int[3];
		//int[] arr = null;
		//Program.acceptRecord(arr);
		System.out.println(Arrays.toString(arr));
	}

	private static void acceptRecord(int[] arr) {
		if(arr!=null)
		{
			for(int index=0;index<arr.length;index++)
			{
				System.out.printf("Enter %d element	:	",index);
				arr[index]=sc.nextInt();
			}
		}		
	}
}
/*
 * Steps for initializing of array
 * int arr = new int[3];
 * int arr = new int[] {10,20,30};
 * int arr = {10,20,30};
 */
