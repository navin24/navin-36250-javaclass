package test;

interface Dictionary<K, V>{
	K getKey( );
	V getValue( );
	void setValue( V value );
}
class HashTable<K, V> implements Dictionary<K,V>{
	private K key;
	private V value;
	public HashTable( K key, V value ) {
		this.key = key;
		this.value = value;
	}
	@Override
	public K getKey() {
		return this.key;
	}
	@Override
	public V getValue() {
		return this.value;
	}
	@Override
	public void setValue(V value) {
		this.value = value;
	}
}
public class Program {
	public static void main(String[] args) {
		Dictionary<Integer, String> d = new HashTable<>( 1, "SunBeam");
		Integer key =  d.getKey();
		String value = d.getValue();
		System.out.println(key+"	"+value);
	}
}
