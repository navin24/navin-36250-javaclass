package test;

import java.util.ArrayList;

public class Program {
	public static ArrayList<Integer> getIntList( ){
		ArrayList<Integer> list = new ArrayList<>();
		list.add(10);
		list.add(20);
		list.add(30);
		return list;
	}
	public static ArrayList<Double> getDoubleList( ){
		ArrayList<Double> list = new ArrayList<>();
		list.add( 10.1 );
		list.add( 20.2 );
		list.add( 30.3 );
		return list;
	}
	public static ArrayList<String> getStringList( ){
		ArrayList<String> list = new ArrayList<>();
		list.add( "DMC" );
		list.add( "DESD" );
		list.add( "DBDA" );
		return list;
	}
	private static void printList(ArrayList<Integer> list) {
		if( list != null ) {
			for( Integer element : list )
				System.out.println(element);
		}
	}
	private static void displayList(ArrayList<Double> list) {
		if( list != null ) {
			for( Double element : list )
				System.out.println(element);
		}
	}
	private static void showList(ArrayList<String> list) {
		if( list != null ) {
			for( String element : list )
				System.out.println(element);
		}
	}
	public static void main(String[] args) {
		ArrayList<Integer> intList = Program.getIntList();
		Program.printList( intList );
		
		ArrayList<Double> doubleList = Program.getDoubleList();
		Program.displayList( doubleList );
		
		ArrayList<String> strList = Program.getStringList();
		Program.showList( strList );
	}
}


