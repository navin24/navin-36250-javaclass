package test;
abstract class A
{
	public final void f1()
	{
		System.out.println("a.f1()");
	}
	public void f2()
	{
		System.out.println("a.f2()");
	}
	public abstract void f3();
};

class B extends A
{
	public final void f2()
	{
		System.out.println("b.f2()");
	}
	@Override
	public void f3() {
		System.out.println("b.f3()");
	}
};
public class Program {

	public static void main(String[] args) {
		A a = new B();
		a.f3();
	}

}
