#include<stdio.h>
class InstanceCounter
{
private:
	static int count;
public:
	InstanceCounter( void )
	{
		InstanceCounter::count = InstanceCounter::count + 1;
	}
	static int getCount( void )
	{
		return InstanceCounter::count;
	}
};
int InstanceCounter::count = 0;
int main( void )
{
	InstanceCounter c1,c2,c3;
	printf("Instance Count	:	%d\n",InstanceCounter::getCount());
	return 0;
}
