#include<stdio.h>
class Test
{
private:
	int num1;			//Instance Variable
	static int num2;	//Class Level Variable
public:
	Test( void )
	{
		this->num1 = 10;
	}
	static void printRecord( void )
	{
		//printf("Num1	:	%d\n", num1);	//Not OK
		Test t;
		printf("Num1	:	%d\n", t.num1);	//Ok
		printf("Num2	:	%d\n",num2);	//OK
	}
};
int Test::num2 = 20;
int main( void )
{
	Test::printRecord();
	return 0;
}
