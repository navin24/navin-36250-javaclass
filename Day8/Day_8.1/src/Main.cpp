#include<stdio.h>
class Test
{
private:
	int num1;			//Instance Variable
	int num2;			//Instance Variable
	static int num3;	//Class Level Variable
public:
	Test( void )
	{
		this->num1 = 0;
		this->num2 = 0;
	}
	//Test *const this = &t
	void setNum1( int num1 )
	{
		this->num1 = num1;
	}
	//Test *const this = &t
	void setNum2( int num2 )
	{
		this->num2 = num2;
	}
	static void setNum3( int num3 )
	{
		Test::num3 = num3;
	}
	//Test *const this = &t
	void printRecord( void )
	{
		printf("Num1	:	%d\n", this->num1 );
		printf("Num2	:	%d\n", this->num2 );
		printf("Num3	:	%d\n", Test::num3);
	}
};
int Test::num3 = 0;
int main( void )
{
	Test t;
	t.setNum1( 10 );	//t.setNum1( &t, 10 );
	t.setNum2( 20 );	//t.setNum2( &t, 20 );
	Test::setNum3( 30 );
	t.printRecord( );	//t.printRecord( &t );
	return 0;
}
