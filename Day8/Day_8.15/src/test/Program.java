package test;

import java.util.Scanner;

public class Program {
	private static void acceptRecord(Complex c1) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Real Number	:	");
		c1.setReal(sc.nextInt());	
		System.out.print("Imag Number	:	");
		c1.setImag(sc.nextInt());	
	}
	private static void printRecord(Complex c1) {
		System.out.println("Real Number	:	"+c1.getReal());
		System.out.println("Imag Number	:	"+c1.getImag());
	}
	public static void main(String[] args) {
		Complex c1 = new Complex( );
		
		Program.acceptRecord( c1 );
		
		Program.printRecord( c1 );
			
	}
}
