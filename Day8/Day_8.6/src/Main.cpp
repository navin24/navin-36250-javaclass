#include<stdio.h>
class	//Anonymous class
{
public:
	void showRecord( void )
	{
		printf("void showRecord( void )\n");
	}
	static void printRecord( void )
	{
		printf("static void printRecord( void )\n");
	}
}t;
int main( void )
{
	t.showRecord();	//OK
	//printRecord( );	//error: use of undeclared identifier 'printRecord'
	//::printRecord( );	//error: no member named 'printRecord' in the global namespace
	t.printRecord();
	return 0;
}
