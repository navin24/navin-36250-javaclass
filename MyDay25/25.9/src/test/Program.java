package test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

public class Program {
	public static void main(String[] args) {
		String searchString = args[ 1 ];
		int count=0;
		try(BufferedReader reader = new BufferedReader(new FileReader(new File(args[0])))){
			String line = null;
			while( ( line = reader.readLine( ) ) != null ) {
				++count;
				if(line.contains(searchString))
					System.out.println(count + " "+line);
			}
		}catch( Exception e) {
			e.printStackTrace();
		}
	}
}

