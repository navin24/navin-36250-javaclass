package test;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class Program {
	public static void main(String[] args) {
		try {
			InetAddress localhost = Inet4Address.getLocalHost();
			String hostName =  localhost.getHostName();
			String hostAddress = localhost.getHostAddress();
			
			System.out.println("Host Name	:	"+hostName);
			System.out.println("Host Address	:	"+hostAddress);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
	}
}
