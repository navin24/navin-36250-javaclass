package test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;

public class Program {
	public static final String pathname = "File.dat";
	public static void writeRecord( ) {
		FileOutputStream outputStream = null;
		try {
			outputStream = new FileOutputStream(new File(pathname));
			for (int ch = 'A';ch<='Z';ch++)			
			outputStream.write(ch);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				outputStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		System.out.println("Record Added");
	}
	public static void readRecord( ) {
		FileInputStream inputStream = null;
		try {
			inputStream = new FileInputStream(new File(pathname));
			int data=0;
			while((data = inputStream.read())!=-1)
			{
				System.out.print((char)data+" ");
			}
			System.out.println();
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				inputStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	}
	public static Scanner sc = new Scanner(System.in);
	public static int menuList( ) {
		System.out.println("0.Exit");
		System.out.println("1.Write Record");
		System.out.println("2.Read Record");
		System.out.print("Enter choice	:	");
		return sc.nextInt();
	}
	public static void main(String[] args) {
		int choice;
		while( ( choice = Program.menuList( ) ) != 0 ) {
			switch( choice ) {
			case 1:
				Program.writeRecord();
				break;
			case 2:
				Program.readRecord();
				break;
			}
		}
	}
}
