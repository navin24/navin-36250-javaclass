package test;

import java.util.Collection;
import java.util.Hashtable;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class Program {
	public static Map<Integer, String> getMap( ){
		Map<Integer, String> map = new Hashtable<Integer, String>( );
		map.put(5,"DAC");
		map.put(1,"DMC");
		map.put(3,"DESD");
		map.put(2,"DBDA");
		map.put(4,"PREDAC");
		return map;
	}
 	public static void main(String[] args) {
 		Map<Integer, String> map = Program.getMap();
 		//System.out.println(map.size());
 		//Program.printKeys( map );
 		//Program.printValues( map );
 		//Program.printKeyValue( map );
 		//Program.removeEntry( map, 2 );
 		//Program.printKeyValue( map );
 		
 		Program.findEntry( map, 2 );
 	}
	private static void findEntry(Map<Integer, String> map, int id) {
		if( map != null ) {
			Integer key = new Integer(id);
			if( map.containsKey(key)) {
				String value = map.get(key);
				System.out.println(key+" "+value+" is found");
			}
			else
				System.out.println(id+" not found");
		}
	}
	private static void removeEntry(Map<Integer, String> map, int id) {
		if( map != null ) {
			Integer key = new Integer(id);
			if( map.containsKey(key)) {
				String value = map.remove(key);
				System.out.println(key+" "+value+" is removed");
			}
			else
				System.out.println(id+" not found");
		}
	}
	private static void printKeyValue(Map<Integer, String> map) {
		if( map != null ) {
			Set<Entry<Integer, String>> entries = map.entrySet();
			for (Entry<Integer, String> entry : entries) {
				System.out.println(entry.getKey()+"	"+entry.getValue());
			}
		}
	}
	private static void printValues(Map<Integer, String> map) {
		if( map != null ) {
			Collection<String> values = map.values();
			for (String value : values)
				System.out.println(value);
		}
	}
	private static void printKeys(Map<Integer, String> map) {
		if( map != null ) {
			Set<Integer> keys = map.keySet();
			for( Integer key : keys )
				System.out.println(key);
		}
	}
}
