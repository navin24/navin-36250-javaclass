package test;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Map.Entry;

class Pair<K,V> implements Entry<K,V>
{
	private int key;
	private int value;
	
	public Pair(int key, int value) {
		this.key = key;
		this.value = value;
	}
	public int getKey() {
		return key;
	}
	public void setKey(int key) {
		this.key = key;
	}
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	
	
}
public class Program {

	public static void main(String[] args) {
		Entry<Integer,String> ent = new Pair(1,"DAC");

	}

}
