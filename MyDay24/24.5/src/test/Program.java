package test;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.Scanner;

public class Program {
	static Scanner sc = new Scanner( System.in );
	public static Account[] getAccounts( ) {
		Account[] arr = new Account[ 5 ];
		arr[ 0 ] = new Account(234,"Current",150000);
		arr[ 1 ] = new Account(728,"Loan",2700000);
		arr[ 2 ] = new Account(351,"Saving",25000);
		arr[ 3 ] = new Account(904,"Joint",45000);
		arr[ 4 ] = new Account(594,"Pention",15000);
		return arr;
	}
	public static Customer[] getCustomers( ) {
		Customer[] arr = new Customer[ 5 ];
		arr[ 0 ] = new Customer("aaa","aaa@gmail.com","11111");
		arr[ 1 ] = new Customer("bbb","bbb@gmail.com","22222");
		arr[ 2 ] = new Customer("ccc","ccc@gmail.com","33333");
		arr[ 3 ] = new Customer("ddd","ddd@gmail.com","44444");
		arr[ 4 ] = new Customer("eee","eee@gmail.com","55555");
		return arr;
	}
	private static void acceptRecord(int[] accNumber) {
		if( accNumber != null ) {
			System.out.print("Enter account number	:	");
			accNumber[ 0 ] = sc.nextInt();
		}
	}
	private static void printRecord(Customer value) {
		if( value != null )
			System.out.println(value.toString());
		else
			System.out.println("Account not found");
	}
	private static void printRecord(boolean removedStatus) {
		if( removedStatus )
			System.out.println("Record is removed");
		else
			System.out.println("Account not found");
	}
	public static int menuList( ) {
		System.out.println("0.Exit");
		System.out.println("1.Add Record");
		System.out.println("2.Find Record");
		System.out.println("3.Remove Record");
		System.out.println("4.Print Record(s)");
		System.out.print("Enter choice	:	");
		return sc.nextInt();
	}
	public static void main(String[] args) {
		int choice;
		
		MapTest test = new MapTest();
		//test.setMap( new Hashtable<Account, Customer>());
		test.setMap(new HashMap<Account,Customer>());
		//test.setMap(new LinkedHashMap<Account,Customer>());
		int[]accNumber = new int[ 1 ];
		while( ( choice = Program.menuList( ) ) != 0 ){
			switch( choice ) {
			case 1:
				Account[] keys = Program.getAccounts();
				Customer[] values = Program.getCustomers();
				test.addRecord( keys, values );
				break;
			case 2:
				Program.acceptRecord( accNumber );
				Customer value = test.findRecord( accNumber[ 0 ] );
				Program.printRecord( value );
				break;
			case 3:
				Program.acceptRecord( accNumber );
				boolean removedStatus = test.removeRecord( accNumber[ 0 ] );
				Program.printRecord( removedStatus );
				break;
			case 4:
				test.printRecords( ); 
				break;
			}
		}
	}
}
