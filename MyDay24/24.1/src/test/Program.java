package test;

import java.util.Dictionary;
import java.util.Enumeration;
import java.util.Hashtable;

public class Program {
	public static Dictionary<Integer, String> getDictionary() {
		Dictionary<Integer, String> d = new Hashtable<>();
		d.put(1, "DAC");
		d.put(2, "DMC");
		d.put(3, "DESD");
		d.put(4, "DBDA");
		d.put(5, "PREDAC");
		return d;
	}
	public static void main(String[] args) {
		Dictionary<Integer, String> d = Program.getDictionary();
		//Program.getDictionary();
		//Program.printKeys( d );
		Program.printValues( d );
		Program.findValue( d );
	}
	private static void findValue(Dictionary<Integer, String> d) {
		if( d != null ) {
			Integer key = new Integer(2);
			String value = d.get(key);
			System.out.println(value);
		}
	}
	private static void printValues(Dictionary<Integer, String> d) {
		if( d != null ) {
			Enumeration<String> values = d.elements();
			while( values.hasMoreElements( ) ) {
				String value = values.nextElement();
				System.out.println(value);
			}
		}
	}
	private static void printKeys(Dictionary<Integer, String> d) {
		if( d != null ) {
			Enumeration<Integer> keys = d.keys();
			Integer key = null;
			while( keys.hasMoreElements()) {
				key = keys.nextElement();
				System.out.println(key);
			}
		}
	}
}