package test;

import java.util.Calendar;

class Time{
	private int hour;	//0
	private int minute;	//0
	private int second;	//0
	private int am_pm;
	public Time( ) {
		Calendar c = Calendar.getInstance();
		//this.day = c.get( Calendar.DATE ); //OK
		this.hour = c.get( Calendar.HOUR_OF_DAY ); //OK
		this.minute = c.get(Calendar.MINUTE);
		this.second = c.get(Calendar.SECOND);
		this.am_pm=c.get(Calendar.AM_PM);
	}
	public void printRecord(String s ) {
		System.out.println(this.hour+"/"+this.minute+"/"+this.second+" "+s);
	}
	public String check()
	{
		String s1="PM";
		String s2="AM";
		if(this.am_pm==1)
		 return s1;
		else
		 return s2;
	}
}
public class Program {
	public static void main(String[] args) {
		String s;
		Time t1=new Time();
		s=t1.check();
		t1.printRecord(s); //OK
	}
	
}
/*package test;

public class Program {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
*/